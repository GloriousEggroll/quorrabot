/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gloriouseggroll.quorrabot.event.irc.channel;

import me.gloriouseggroll.quorrabot.twitchchat.Channel;
import me.gloriouseggroll.quorrabot.twitchchat.Session;

/**
 *
 * @author gloriouseggroll
 */
public class IrcChannelUsersUpdateEvent extends IrcChannelEvent {
    private final String[] joins;
    private final String[] parts;

    /*
     * Class constructor.
     *
     * @param {TwitchSession}  session
     * @param {String[]} joins
     * @param {String[]} parts
     */
    public IrcChannelUsersUpdateEvent(Session session, Channel channel, String[] joins, String[] parts) {
        super(session,channel);

        this.joins = joins;
        this.parts = parts;
    }

    /*
     * Class constructor.
     *
     * @param {String[]} joins
     * @param {String[]} parts
     */
    public IrcChannelUsersUpdateEvent(String[] joins, String[] parts) {
        super(null,null);

        this.joins = joins;
        this.parts = parts;
    }

    /*
     * Method that returns the current array of users who joined the channel in the last 10 minutes.
     *
     * @return {String[]} joins
     */
    public String[] getJoins() {
        return this.joins;
    }

    /*
     * Method that returns the current array of users who left the channel in the last 10 minutes.
     *
     * @return {String[]} parts
     */
    public String[] getParts() {
        return this.parts;
    }
}
