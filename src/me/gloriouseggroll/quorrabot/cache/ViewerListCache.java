/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gloriouseggroll.quorrabot.cache;

import com.gmt2001.TwitchAPIv5;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;

import me.gloriouseggroll.quorrabot.event.irc.channel.IrcChannelUsersUpdateEvent;
import me.gloriouseggroll.quorrabot.event.EventBus;


public class ViewerListCache implements Runnable {
    private static ViewerListCache instance = null;
    private final String channelName;
    private final Thread thread;
    private List<String> cache = new ArrayList<String>();
    private boolean isKilled = false;

    /*
     * Method to get this instance.
     *
     * @param  {String} channelName
     * @return {Object}
     */
    public static ViewerListCache instance(String channelName) {
        if (instance == null) {
            instance = new ViewerListCache(channelName);
        }

        return instance;
    }

    /*
     * Class constructor.
     *
     * @param  {String} channelName
     */
    private ViewerListCache(String channelName) {
        Thread.setDefaultUncaughtExceptionHandler(com.gmt2001.UncaughtExceptionHandler.instance());

        this.channelName = channelName;

        this.thread = new Thread(this, "tv.phantombot.cache.ViewerListCache");
        this.thread.setDefaultUncaughtExceptionHandler(com.gmt2001.UncaughtExceptionHandler.instance());
        this.thread.start();
    }

    /*
     * Method that updates the cache every 10 minutes.
     */
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        while (!isKilled) {
            try {
                try {
                    this.updateCache();
                } catch (Exception ex) {
                    com.gmt2001.Console.debug.println("ViewerListCache::run: " + ex.getMessage());
                }
            } catch (Exception ex) {
                com.gmt2001.Console.err.println("ViewerListCache::run: " + ex.getMessage());
            }

            try {
                Thread.sleep(600 * 1000);
            } catch (InterruptedException ex) {
                com.gmt2001.Console.err.println("ViewerListCache::run: Failed to execute sleep [InterruptedException]: " + ex.getMessage());
            }
        }
    }

    /*
     * Method that updates the cache.
     */
    public void updateCache() throws Exception {
        String[] types = new String[] { "moderators", "staff", "admins", "global_mods", "viewers" };
        List<String> cache = new ArrayList<String>();
        List<String> joins = new ArrayList<String>();
        List<String> parts = new ArrayList<String>();

        com.gmt2001.Console.debug.println("ViewerListCache::updateCache");
        try {
            JSONObject object = TwitchAPIv5.instance().GetChatUsers(channelName);
            JSONObject chatters;

            if (object.getBoolean("_success") && object.getInt("_http") == 200) {
                if (object.getInt("chatter_count") == 0) {
                    this.cache = cache;
                    return;
                }

                // Add the new chatters to a new cache.
                chatters = object.getJSONObject("chatters");
                for (String type : types) {
                    JSONArray array = chatters.getJSONArray(type);
                    for (int i = 0; i < array.length(); i++) {
                        cache.add(array.getString(i));
                    }
                }

                // Check for new users that joined.
                for (int i = 0; i < cache.size(); i++) {
                    if (!this.cache.contains(cache.get(i))) {
                        joins.add(cache.get(i));
                    }
                }

                // Check for old users that left.
                for (int i = 0; i < this.cache.size(); i++) {
                    if (!cache.contains(this.cache.get(i))) {
                        parts.add(this.cache.get(i));
                    }
                }

                EventBus.instance().post(new IrcChannelUsersUpdateEvent(joins.toArray(new String[joins.size()]), parts.toArray(new String[parts.size()])));
                // Set the new cache.
                this.cache = cache;
                // Delete the temp caches.
                cache = null;
                parts = null;
                joins = null;
                // Run the GC to clear memory,
                System.gc();
            } else {
                com.gmt2001.Console.debug.println("Failed to update viewers cache: " + object);
            }
        } catch (Exception ex) {
            com.gmt2001.Console.debug.println("ViewerListCache::updateCache: Failed to update: " + ex.getMessage());
        }
    }

    /*
     * Method to check if a user is in the cache.
     *
     * @param  {String} username
     * @return {Boolean}
     */
    public boolean hasUser(String username) {
        return (!this.cache.isEmpty() ? this.cache.contains(username) : true);
    }

    /*
     * Method to add users to the cache.
     *
     * @param  {String} username
     */
    public void addUser(String username) {
        this.cache.add(username);
    }

    /*
     * Method to kill this cache.
     */
    public void kill() {
        this.isKilled = true;
    }
}
