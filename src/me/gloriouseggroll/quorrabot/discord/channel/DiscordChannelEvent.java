/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gloriouseggroll.quorrabot.discord.channel;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import me.gloriouseggroll.quorrabot.discord.DiscordEvent;

public abstract class DiscordChannelEvent extends DiscordEvent {

    /*
     * Abstract constructor.
     *
     * @param {IUser} user
     */
    protected DiscordChannelEvent(IUser user) {
        super(user);
    }

    /*
     * Abstract constructor.
     *
     * @param {IUser}    user
     * @param {IChannel} channel
     */
    protected DiscordChannelEvent(IUser user, IChannel channel) {
        super(user, channel);
    }
}