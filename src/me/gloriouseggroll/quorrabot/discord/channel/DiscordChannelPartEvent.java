/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gloriouseggroll.quorrabot.discord.channel;
import sx.blah.discord.handle.obj.IUser;


public class DiscordChannelPartEvent extends DiscordChannelEvent {

    /*
     * Class constructor.
     *
     * @param {IUser} user
     */
    public DiscordChannelPartEvent(IUser user) {
        super(user);
    }
}
