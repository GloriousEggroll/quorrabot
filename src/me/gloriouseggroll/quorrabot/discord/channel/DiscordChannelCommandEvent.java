/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gloriouseggroll.quorrabot.discord.channel;

import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IChannel;

import java.util.LinkedList;
import java.util.List;

public class DiscordChannelCommandEvent extends DiscordChannelEvent {
    private final String arguments;
    private final boolean isAdmin;
    private final String[] args;
    private String command;

    /*
     * Class constructor for this event.
     *
     * @param {IUser}    user
     * @param {IChannel} channel
     * @param {String}   command
     * @param {String}   arguments
     * @param {boolean}  isAdmin
     */
    public DiscordChannelCommandEvent(IUser user, IChannel channel, String command, String arguments, boolean isAdmin) {
        super(user, channel);

        this.command = command;
        this.arguments = arguments;
        this.isAdmin = isAdmin;
        this.args = parse();
    }

    /*
     * Method that parses the command arguments.
     *
     * @return {String[]}
     */
    private String[] parse() {
        List<String> tempArgs = new LinkedList<String>();
        Boolean hasQuote = false;
        String tempString = "";

        for (char c : arguments.toCharArray()) {
            if (c == '"') {
                hasQuote = !hasQuote;
            } else if (!hasQuote && c == ' ') {
                if (tempString.length() > 0) {
                    tempArgs.add(tempString);
                    tempString = "";
                }
            } else {
                tempString += c;
            }
        }

        if (tempString.length() > 0) {
            tempArgs.add(tempString);
        }

        return tempArgs.toArray(new String[tempArgs.size()]);
    }

    /*
     * Method that sets the command for this class. Mostly used for aliases.
     *
     * @return {String} command
     */
    public String setCommand(String command) {
        this.command = command;
        return this.command;
    }

    /*
     * Method that returns the command.
     *
     * @return {String} command
     */
    public String getCommand() {
        return this.command.toLowerCase();
    }

    /*
     * Method that returns the string of arguments.
     *
     * @return {String} arguments
     */
    public String getArguments() {
        return this.arguments;
    }

    /*
     * Method that returns the array of arguments
     *
     * @return {String[]} args
     */
    public String[] getArgs() {
        return this.args;
    }

    /*
     * Method that returns if the user a admin in the server.
     *
     * @retrun {boolean}
     */
    public boolean isAdmin() {
        return this.isAdmin;
    }

    /*
     * Method that returns this object as a string.
     *
     * @return {String}
     */
    @Override
    public String toString() {
        return "DiscordChannelCommandEvent -> { command: [" + this.command + "] arguments: [" + this.arguments + "] isAdmin: [" + this.isAdmin + "] }";
    }
}
